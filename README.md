# exo_editable



## Installation

1. Installer un serveur sur son ordinateur
2. Installer GitLab et se créer un compte GitLab
3. Cloner git "exo_editable" à la racine de votre server.
4. ouvrir dossier depuis "localhost//exo_editable/test/"**


## Composant

**Le composant est une fonction qui prends en compte les sources vers les fichier "JSON", qui créer des tableaux affichent des valeurs à partir de fichiers "JSON" aussi permettre rendre les données éditable ,trier des données sur chaque colonne  (ordre alphanumérique) et filtrer des données.**

## Ajouter un nouveau fichier JSON

_il faut ajouter un nouveau fichier JSON au dossier "data" et ajouter une nouvelle "option" au html_

 ## Code JS
        var myselect = document.getElementById("myselect")_  **// je déclare ma variables**
        myselect.addEventListener("change", editable)_  **//permettre gérer l'événement onchange sur <select> en appelant la fonction "editable"_**
                    
        function editable(){   **// Function qui permetre choisir plusieur fichier JSON**
        let monURL = '../data/'+myselect.value+'.json'   ** // je charger les sources des fichiers JSON que je veux récupérer dans une variable en mélangeant avec chain de character_**
                                                
## HTML

        IL faut : 
        créer une balise <select> qui a id «myselect »   et contient  les <option> 
        une balise <table> qui a id "container"
        La balise table contient la balise <thead> 
        La balise <thead>  contient la balise <tr> qui a id « entete »
        et  une balise <tbody> qui a id « tab »
        
## code HTML
        <select id="myselect">
                    <option value="">...</option>
            </select>
             <table id="container">
        <thead>
            <tr id="entete"></tr>
        </thead>
            <tbody id="tab"> </tbody>
    </table>
