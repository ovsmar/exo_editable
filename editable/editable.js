var myselect = document.getElementById("myselect") //!<= je déclare ma variables
myselect.addEventListener("change", editable) //!<= permettre gérer l'événement onchange sur <select> en appelant la fonction "editable"
            
function editable(){  //!<= Function qui permetre choisir plusieur fichier JSON
let monURL = '../data/'+myselect.value+'.json'   //!<= je charger les sources des fichiers JSON que je veux récupérer dans une variable en mélangeant avec chain de character                                             
let monTab;

fetch(monURL)   //!<= j'envoie ma requête
    .then(     //!<= quand je reçois la réponse
        (response) => response.json()   //!<= je récupère le JSON de la réponse
    )
    .then(                            //!<= quand c'est pret
        (tab) => {
            monTab = tab;             //!<= je mets mes data dans ma variable  
            creerMonTableauHtml();   //!<= je lance ma fonction d'affichage 
            filtre()            //!<= je lance ma fonction de filter
        }
    ).catch(
        (err) => {
            console.log(err);     //!<= j'affiche les erreurs HTTP au cas où
        }
    );

        let entete=document.getElementById("entete")
        let tab=document.getElementById("tab")
    function creerMonTableauHtml() { //!<= Ma  fonction qui cree tableu html
  
       
        entete.innerHTML='';
        for (let nomCategorie in monTab[0]) {  //!<== boucle 1
            var headTable = document.createElement("th");
            headTable.id = "thead"+ nomCategorie;
            headTable.className = "thead"+ nomCategorie;
            headTable.innerHTML = nomCategorie;
            entete.appendChild(headTable);

        }
        tab.innerHTML="";
        for (let info in monTab){   //!<== boucle2
            var bodyTable = document.createElement("tr");
            bodyTable.id = "tbody" + info["id"];
            bodyTable.className = "tbody";
            tab.appendChild( bodyTable);
            bodyTable.setAttribute("contenteditable", "true")  //!<== je rendre mes données éditable
                 
        for ( let donnes in monTab[info]){   //!<== boucle3
            var bodyTD = document.createElement("td");
            bodyTD.innerHTML = monTab[info][donnes];
            bodyTD.name = monTab[info][donnes];
            bodyTable.appendChild(bodyTD);

             }  
               
         } 
     
    }   
    document.getElementById('buttonControl').innerHTML=""
    let ajouterLigne = document.createElement("button");   //!<== create button
        ajouterLigne.textContent = "+";
        document.getElementById('buttonControl').appendChild(ajouterLigne)
        ajouterLigne.onclick = function () {
                                                    //!<== pour ajouter une ligne dans mon tableu
        bodyTable = document.createElement("tr");
        bodyTable.setAttribute("info", "bodyTable");
        bodyTable.setAttribute("contenteditable", "true"); //!<== je rendre mes données éditable
        tab.appendChild(bodyTable);
  
      for (let i in monTab[0]) {
        let donnes = document.createElement("td");
        donnes.setAttribute("info", "donnes");
        donnes.innerText = "New row";
        bodyTable.appendChild(donnes);
      }
    
    };
 }
 
 
  function filtre() { //!<= Ma fonction filtre

    var input, filter, table, tr, td, i; //!<= je déclare mes variables
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("tab");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {   //!<= je parcoure toutes mes lignes du tableau 
      var cells = rows[i].getElementsByTagName("td");
      var j;
      var rowContainsFilter = false;
      for (j = 0; j < cells.length; j++) {
        if (cells[j]) {
          if (cells[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
            rowContainsFilter = true;
            continue;
          }
        }
      }
  
      if (! rowContainsFilter) {
        rows[i].style.display = "none"; //!<= je metre en display "none" celles qui ne correspondent pas à la requête de recherche
      } else {
        rows[i].style.display = "";
      }
    }
  };


let theads = document.querySelectorAll("#container thead");
theads.forEach(thead => thead.addEventListener("click",trier));

function trier(e) {  //!<= Ma fonction trier
    let target = e.target
    let order = (target.dataset.order = -(target.dataset.order || -1));
    let thList = Array.from(target.parentNode.cells);
    let index = thList.indexOf(target);
    let collator = new Intl.Collator(["en"], { numeric: true });
    let comparator = (index, order) => (a, b) => {   //!<= Comparaison...
       return (
              order * collator.compare(a.children[index].innerHTML,b.children[index].innerHTML)   
    );
 };
	let tablesBodies = Array.from(target.closest("table").tBodies);
	
	tablesBodies.forEach(tBody => {
		
		tBody.append(...Array.from(tBody.rows).sort(comparator(index, order)));
	});

	thList.forEach( th => th.classList.toggle("sorted", th === target));
}